import { createPinia } from 'pinia';
import type { UserModule } from './wrapper';

export const install: UserModule = ({ use }) => {
	use(createPinia())
}
