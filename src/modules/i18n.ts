import type { UserModule } from './wrapper';
import { createI18n } from 'vue-i18n'

const messages = Object.fromEntries(
	Object.entries(
		import.meta.glob<{ default: any }>('../../locales/*.y(a)?ml', { eager: true }))
		.map(([key, value]) => {
			const yaml = key.endsWith('.yaml')
			return [key.slice(14, yaml ? -5 : -4), value.default]
		}),
)

export const install: UserModule = ({ use }) => {
	const i18n = createI18n({
		legacy: false,
		locale: 'sl-SI',
		fallbackLocale: 'en',
		messages
	})

	use(i18n);
}
