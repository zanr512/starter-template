import { createRouter, createWebHistory } from 'vue-router';
import { setupLayouts } from 'virtual:generated-layouts';
import generatedRoutes from 'virtual:generated-pages';
import type { UserModule } from './wrapper';
const routes = setupLayouts(generatedRoutes);

export const install: UserModule = ({ use }) => {
	const router = createRouter({
		history: createWebHistory(import.meta.env.BASE_URL),
		routes,
	});
	use(router);
}
