import 'vite/modulepreload-polyfill'
import { createApp } from 'vue';

import App from './App.vue';

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import './assets/main.css';
import type { UserModule } from './modules/wrapper';

const app = createApp(App);

app.use(ElementPlus)


Object.values(
	import.meta.glob<{ install: UserModule }>('./modules/*.ts', { eager: true })
).forEach((i) => i.install?.(app));

app.mount('#app');

