import { fileURLToPath, URL } from 'node:url';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import Pages from 'vite-plugin-pages';
import Layouts from 'vite-plugin-vue-layouts';
import Components from 'unplugin-vue-components/vite';
import AutoImport from 'unplugin-auto-import/vite';
import VueI18n from '@intlify/vite-plugin-vue-i18n'
import path, { resolve } from 'node:path';
import copy from 'rollup-plugin-copy'


// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		vue(),
		Components({
			// allow auto load markdown components under `./src/components/`
			extensions: ['vue' /*, 'md'*/],
			// allow auto import and register components used in markdown
			include: [/\.vue$/, /\.vue\?vue/, /*/\.md$/*/],
			dts: 'src/components.d.ts',
		}),
		AutoImport({
			imports: [
				'vue',
				'vue-router',
				'vue-i18n',
				//'vue/macros',
				//'@vueuse/head',
				//'@vueuse/core',
			],
			dts: 'src/auto-imports.d.ts',
			dirs: ['src/composables', 'src/stores'],
			vueTemplate: true,
		}),
		Pages({
			extensions: ['vue', /*'md'*/],
		}),
		Layouts(),
		VueI18n({
			runtimeOnly: true,
			compositionOnly: true,
			include: [path.resolve(__dirname, 'locales/**')],
		}),
		copy({
			hook: 'writeBundle',
			targets: [{
				src: 'dist/assets/app.*.js',
				dest: 'dist/lol.js'
			}]
		})
	],
	resolve: {
		alias: {
			'@': fileURLToPath(new URL('./src', import.meta.url)),
		},
	},
	build: {
		rollupOptions: {
			input: {
				app: resolve(__dirname, 'src/apps/app1/main.ts'),
				app1: resolve(__dirname, 'src/apps/app2/index.html'),
				csslol: resolve(__dirname, 'src/assets/main.css')
			},
			//external: ['vue'],
			output: {
				// Provide global variables to use in the UMD build
				// for externalized deps
				globals: {
					//vue: 'Vue'
				}
			},
		},
		manifest: true,
		sourcemap: true,

	},
	server: {
		proxy: {
			'*': {
				target: 'https://localhost:7195/',
				changeOrigin: true
			}
		},
		hmr: {
			protocol: 'ws'
		}
	},
	appType: 'mpa',
});

// export default defineConfig({
// 	build: {
// 		rollupOptions: {
// 			input: {
// 				skin: 'src/assets/main.css',
// 				skinScss: 'src/assets/main.scss'
// 			}
// 		}
// 	},
// 	css: {
// 		postcss: {
// 			map: true,
// 			plugins: [
// 				cssnano({
// 					preset: 'default'
// 				})
// 			]
// 		}
// 	}
// })
